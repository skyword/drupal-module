This branch should not be used.

Please refer to the correct branch for your version of Drupal. For example:

Drupal 8 uses branch 8.x-1.x as of 2018 Apr 19
Drupal 7 uses branch 7.x-1.x as of 2018 Apr 19

There may be a more recent module version than the tracked branch, so you should verify all available branches before assuming these are correct.
